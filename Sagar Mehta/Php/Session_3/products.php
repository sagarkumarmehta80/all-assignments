<html>
<head>
<title>Products</title>
<link href="stylesheets/product_style.css" rel="stylesheet" type="text/css">
<script src="jquery.js"></script>
<script src="https://use.fontawesome.com/a23f17d24e.js"></script>

</head>
<body>
    <script>
        $(document).ready(function(){
            $(".cols").click(function(){
              let p_name=$(this).find("#p_name").text().trim().toLowerCase();
              let p_code=$(this).find("#p_code").text().trim().toLowerCase();
              let p_price=$(this).find("#p_price").text().trim().toLowerCase();
              let p_avl=$(this).find("#p_avl").text().trim().toLowerCase();
            // console.log(p_name+p_code+p_avl.substr(0,1)+p_price);
            $("input[name=p_name]").val(p_name);
            $("input[name=p_price]").val(p_price);
            $("input[name=p_code]").val(p_code);
            $("select[name=p_avl]").val(p_avl.substr(0,1));
            $(".update_data").show();
            });
            $(".close").click(function(e){
                e.preventDefault();
                $(".update_data").hide();
               
            });
            $("input[type=submit]").click(function(e){
                e.preventDefault();
                 let p_name_txt=$("input[name=p_name]").val();
                 let p_price_txt=$("input[name=p_price]").val();
                 let p_avl_txt=$("select[name=p_avl]").val();
                 let p_code_txt=$("input[name=p_code]").val();
                 let p_img_txt=$("input[name=p_file]").val();
                 console.log(p_name_txt+p_price_txt+p_code_txt+p_avl_txt+p_img_txt);
                var all_data={p_name:p_name_txt,p_price:p_price_txt,p_avl:p_avl_txt,p_code:p_code_txt,p_img:p_img_txt};
                 $.ajax({
                        method:'post',
                        url:"product_update.php",
                        data:all_data,
                        success:function(rslt){

                            alert(rslt);
                            window.location.replace('products.php');
                            },
                        error:function(err){alert(err)}
                 });
            });





            $("button[name=dlt]").click(function(e){
                e.preventDefault();
                var cnf=confirm("Are You Sure?");
                if(cnf==true)
                {
                 let dlt_p_code_txt=$("input[name=p_code]").val();
                //  console.log(p_name_txt+p_price_txt+p_code_txt+p_avl_txt+p_img_txt);
                 $.ajax({
                        method:'post',
                        url:"product_delete.php",
                        data:{dlt_p_code:dlt_p_code_txt},
                        success:function(rslt){

                            alert(rslt);
                            window.location.replace('products.php');
                            },
                        error:function(err){alert(err)}
                 });
                }
            });
        });
        </script>
    <h2>product management</h2>
<?php
include 'connect.php';

    $query="select * from products;";

    $retrv=mysqli_query($conn,$query);

  if(mysqli_num_rows($retrv)>0)
  {
      echo "<div class='container'>";
    while($row=mysqli_fetch_assoc($retrv))
    {
        if($row['is_active']=='y')
        {
            $avlbl="yes";
        }
        else
        {
            $avlbl="no";
        }
        echo "<div class='cols'><span class='img blk'><img src='".$row["images"]."'/></span><span id='p_name'>".$row["name"]."</span> (&#8377;<span id='p_price'>".$row["price"]."</span>)<span class=blk>product code:<span id='p_code'> ".$row["product_code"]."</span></span><span class=blk>available:<span id='p_avl'> ".$avlbl."</span></span></div><br><br>";
    }
    echo "</div>";
}
    else
    {
        echo "0 results";
        }


?>


<div class="update_data">
<form enctype='multipart/form-data' method='post'>
    <input type="hidden" name="p_code">
<table>
    <!-- <tr> <td>product image:</td><td><input type=file name=p_file></td></tr> -->
        <tr><td>product name:</td><td><input type=text name=p_name></td></tr>
        <tr><td>product price:</td><td><input type=number name=p_price></td></tr>
        <tr><td>product available:</td><td><select name="p_avl"><option value="y">yes</option> <option value="n">no</option></select> </td></tr>
        <tr><td><input type=submit></td><td><button name="dlt">delete item</button></td></tr>
        <tr><td colspan=2><button class="close">X close</button></td></tr>
    </table>
    </form>

</div>
</body>
</html>